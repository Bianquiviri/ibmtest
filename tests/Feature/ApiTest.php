<?php

namespace Tests\Feature;

use App\Models\sum;
use Faker\Generator as Faker;
use Tests\TestCase;

class ApiTest extends TestCase
{
    /**
     * A basic test example.
     *
     * @return void
     */
    public function test_istAlive()
    {
        $response = $this->get('/');

        $response->assertStatus(200);
    }

    public function test_making_an_api_request()
    {
        $response = $this->getJson('/api/sum');
        $response->assertStatus(200);
    }

    public final function test_get_details_of_api_request()
    {
        $response = $this->getJson('/api/sum/1');
        $response->assertStatus(200);
    }

    public function test_create_new_sum_api_request()
    {
        $response = $this->postJson('/api/sum/', [
            'sumando01' => $this->uniqueNumberInDataBase('sumando01'),
            'sumando02' => $this->uniqueNumberInDataBase('sumando02'),
        ]);
        $response->assertStatus(200)
            ->assertJson([
                'result' => true
            ]);
    }

    private function uniqueNumberInDataBase($rowname)
    {
        $faker = new faker();
        $number = '';
        do {
            $number = $faker->randomNumber();
        } while (sum::where($rowname, $number)->first());
        return $number;
    }
}
