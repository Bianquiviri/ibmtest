<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class Tablessums extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        // to create tables sums
        Schema::create('sums', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->integer('sumando01')->unique();
            $table->integer('sumando02')->unique();
            $table->integer('result');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
        Schema::dropIfExists('sums');
    }
}
