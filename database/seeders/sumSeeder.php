<?php

namespace Database\Seeders;

use App\Models\sum;
use Faker\Generator as Faker;
use Illuminate\Database\Seeder;

class sumSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $sumando01 = $this->uniqueNumberInDataBase('sumando01');
        $sumando02 = $this->uniqueNumberInDataBase('sumando02');
        $dataSum = [
            [
                'sumando01'  => $sumando01,
                'sumando02'  => $sumando02,
                'result'     => $sumando01 + $sumando01,
                'updated_at' => now(),
                'created_at' => now(),
            ],
        ];

        sum::insert($dataSum);
    }

    private function uniqueNumberInDataBase($rowname)
    {
        $faker = new faker();
        $number = '';
        do {
            $number = $faker->randomNumber();
        } while (sum::where($rowname, $number)->first());
        return $number;
    }
}
