<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class sum extends Model
{
    use HasFactory;
    protected $fillable =['id','sumando01','sumando02','result','updated_at','created_at'];

}
