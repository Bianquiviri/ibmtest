<?php

namespace App\Http\Controllers;

use App\Http\Requests\sumrequest;
use App\Models\sum;
use Illuminate\Http\Request;

class SumController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        try {
            return response()->json(sum::get(), 200);
        } catch (\Exception $ext) {
            return response()->json($ext->getMessage(), 500);
        }
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function store(sumrequest $request)
    {
        // to store a add
        try {

            $newadd = new sum();
            $newadd->sumando01 = $request->sumando01;
            $newadd->sumando02 = $request->sumando02;
            $newadd->result = $request->sumando01 + $request->sumando02;
            $newadd->save();

            return response()->json(['result' => $newadd], 200);

        } catch (\Exception $ext) {
            return response()->json(['error' => $ext->getMessage(), 500]);

        }
    }

    /**
     * Display the specified resource.
     *
     * @param \App\Models\sum $sum
     * @return \Illuminate\Http\Response
     */
    public function show(sum $sum)
    {
        try {
            return response()->json(['data', $sum], 200);
        } catch (\Exception $ext) {
            return response()->json(['error', $ext->getMessage()]);

        }
    }

    /**
     * Update the specified resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @param \App\Models\sum $sum
     * @return \Illuminate\Http\Response
     */
    public function update(sumrequest $request, sum $sum)
    {
        //
        try {

            $sum->sumando01 = $request->sumando01;
            $sum->sumando02 = $request->sumando02;
            $sum->result = $request->sumando01 + $request->sumando02;
            $sum->save();

            return response()->json(['data' => $sum->get()], 202);

        } catch (\Exception $ext) {
            return $ext->getMessage();
            # return response()->json(['error' => $ext->getMessage()],500);
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param \App\Models\sum $sum
     * @return \Illuminate\Http\Response
     */
    public function destroy(sum $sum)
    {
        try {
            sum::findOrFail($sum)->delete();
            return response()->json('info', 'El item fue eliminado');
        } catch (\Exception $ext) {
            return response()->json(['error', $ext->getMessage(), 404]);
        }
    }
}
