<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class sumrequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'sumando01' => 'required|integer|unique:sums',
            'sumando02' => 'required|integer|unique:sums',
        ];
    }

    public function messages()
    {
        return [
            'sumando01.required' => 'El valor de sumando01 es ogligatorio',
            'sumando01.integer' => 'El valor de sumando01 debe ser entero',
            'sumando01.unique' => 'El valor de sumando01 debe ser unico',
            'sumando02.required' => 'El valor de sumando02 es requerido',
            'sumando02.unique' => 'El valor de sumando01 debe ser unico',
            'sumando02.integer' => 'El valor de sumando02 debe ser entero',
        ];
    }
}
