#!/usr/bin/env bash
rm -rf vendor
echo "Ejecutando Composer"
composer install;
echo "Activando .env"
cp .env.example .env;
echo "Levantado los contenedores"
./vendor/bin/sail up -d;
echo "Generando key app"
./vendor/bin/sail php artisan key:generate;
echo "Ejecutando migraciones y seed"
./vendor/bin/sail php artisan migrate --seed
echo "Ejecutando test"
./vendor/bin/sail php artisan test
